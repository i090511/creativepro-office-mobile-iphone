exports.getFilesForTask = function(taskID) {
	var fileAJAX = Titanium.Network.createHTTPClient();
	fileAJAX.onload = function() {
		var data = this.responseText;
		var jdata = JSON.parse(data);
		if (jdata.Error == '1') {
			alert('Uh oh! Data retrieval error.');
		} else {
			var data = [],row;
			for (var fileIndex = 0; fileIndex < (jdata.Files.length - 1); fileIndex++ ) {
				fileObj = jdata.Files[fileIndex];
				var fileIcon = getFileIcon(fileObj.FileClassName);
				var fileSize = fileSizeFormat(fileObj.FileSize);
				
				var fileDate = globals.moment(fileObj.DateEntry, 'YYYY-MM-DDTHH:mm:ss');
				fileDate = fileDate.format('MMM Do YYYY');
				var fileStats = fileObj.UploaderName+' on '+fileDate+' '+fileSize;
				
				row = Ti.UI.createTableViewRow({ height: 43, fileLink: fileObj.fileLink, hasChild: false });
				l1 = Ti.UI.createLabel({
					text: fileObj.FileNameActual,
					top: -17, 
					left: 48, 
					font: {fontSize: 16}
				});
				l2 = Ti.UI.createLabel({
					backgroundImage: fileIcon,
					width: 32,
					height: 32,
					left: 8,
				});
				l3 = Ti.UI.createLabel({
					text: fileStats,
					color: '#999',
					font: { fontSize: 13 },
					top: 21,
					left: 48
				});
				row.add(l1,l2,l3);
				data.push(row);
			}	
			tables.fileTable.setData(data);
		}	
	}	
	fileAJAX.open('POST',globals.SITE_URL+'files/FileManager/getFilesForSomething/'+taskID+'/task/json/0');
	fileAJAX.send({
		'authKey': globals.AUTH_KEY
	});	 
}

function fileSizeFormat(filesize) {
	filesize = parseInt(filesize);
	if (filesize >= 1073741824) {
	     filesize = globals.utils.number_format(filesize / 1073741824, 2, '.', '') + ' Gb';
	} else if (filesize >= 1048576) {
     	filesize = globals.utils.number_format(filesize / 1048576, 2, '.', '') + ' Mb';
   	} else if (filesize >= 1024) {
    	filesize = globals.utils.number_format(filesize / 1024, 0) + ' Kb';
  	} else {
		filesize = globals.utils.number_format(filesize, 0) + ' bytes';
	}
  return filesize;
}

function getFileIcon(fileType) {
	var fileIcon;
	switch(fileType) {
		case 'icon_file_pdf':
			fileIcon = 'images/mime/page_white_acrobat.png'
			break;
		case 'icon_file_jpg':
			fileIcon = 'images/mime/page_white_picture.png'
			break;	
		case 'icon_file_gif':
			fileIcon = 'images/mime/page_white_picture.png'
			break;	
		case 'icon_file_png':
			fileIcon = 'images/mime/page_white_picture.png'
			break;	
		case 'icon_file_tif':
			fileIcon = 'images/mime/page_white_picture.png'
			break;		
		case 'icon_file_bmp':
			fileIcon = 'images/mime/page_white_picture.png'
			break;			
		case 'icon_file_psd':
			fileIcon = 'images/mime/page_white_vector.png'
			break;		
		case 'icon_file_mp3':
			fileIcon = 'images/mime/sound.png'
			break;		
		case 'icon_file_doc':
			fileIcon = 'images/mime/page_white_word.png'
			break;		
		case 'icon_file_txt':
			fileIcon = 'images/mime/page_white_text.png'
			break;	
		case 'icon_file_rtf':
			fileIcon = 'images/mime/page_white_text.png'
			break;		
		case 'icon_file_zip':
			fileIcon = 'images/mime/page_white_zip.png'
			break;					
		case 'icon_file_wma':
			fileIcon = 'images/mime/film.png'
			break;					
		case 'icon_file_wav':
			fileIcon = 'images/mime/sound.png'
			break;
		case 'icon_file_ppt':
			fileIcon = 'images/mime/page_white_powerpoint.png'
			break;
		case 'icon_file_xls':
			fileIcon = 'images/mime/page_white_excel.png'
			break;		
		case 'icon_file_xml':
			fileIcon = 'images/mime/page_white_code.png'
			break;								
		default:
			fileIcon = 'images/mime/page_white.png'	
	}
	return fileIcon;	
}
