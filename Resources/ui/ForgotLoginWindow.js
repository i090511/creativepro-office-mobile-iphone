exports.ForgotLoginWindow = function(args) {
	var instance = Ti.UI.createWindow(args);
	
	var btnCancel = Titanium.UI.createButton({
		title: 'Login',
		style:Titanium.UI.iPhone.SystemButtonStyle.DONE
	});
	
	btnCancel.addEventListener('click', function() {
		globals.loginTab.setActiveTab(0);
	});
	
	var email = Titanium.UI.createTextField({
	    hintText : 'Email address',
	    keyboardType: Titanium.UI.KEYBOARD_EMAIL,
	    height : 35,
	    width : 300,
	    top : 10,
	    borderStyle : Titanium.UI.INPUT_BORDERSTYLE_ROUNDED,
	    returnKeyType: Titanium.UI.RETURNKEY_NEXT
	});
	
	var btnFind = Ti.UI.createButton({
		title: 'Find my password',
		color: '#2873ba',
		backgroundImage: 'none',
		borderRadius: 8,
		borderColor: '#3787d3',
		borderWidth: 1,
		font: {
			fontSize: 22,
			fontWeight: 'bold'			
		},
		selectedColor: 'none',
		selectedImage: 'none',
	    backgroundGradient: globals.styles.gradient('ltBlue'),
		height: 50,
		width: 300,
		top: 55,
		left: 10
	});
	
	btnFind.addEventListener('click',function(e) {
   		submitFindForm();
	});
	
	function submitFindForm() {
		var emailValue = email.getValue();
   		
   		if (emailValue == '') {
   			alert('Please enter an email address.');
   		} else {
   			globals.login.findPassword(emailValue);
   			email.blur();
   		}
	}
	
	instance.add(email);
	instance.add(btnFind);
	instance.setLeftNavButton(btnCancel);
	
	instance.backButtonTitleImage = null;	
	instance.backButtonTitle = 'It Worked!';

	return instance;
};