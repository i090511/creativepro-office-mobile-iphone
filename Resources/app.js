// this sets the background color of the master UIView (when there are no windows/tab groups on it)
Titanium.UI.setBackgroundColor('#000');
require('lib/requirePatch').monkeypatch(this);
var globals  = {};
var controls = {};
var labels   = {};
var tables   = {};

//create a private scope to prevent further polluting the global object
(function() {
	/*
	 * Use this to test with different user accounts.
	 */
	globals.AUTH_STRING = 'CMLxBF8taxI797EK45HJ';
	
	var AppTabGroup       = require('ui/AppTabGroup');
	var	LoginWindow       = require('ui/LoginWindow');
	var ForgotLoginWindow = require('ui/ForgotLoginWindow');
	var database          = require('lib/database');
	globals.login         = require('lib/login');
	globals.styles        = require('lib/styles');
	globals.utils         = require('lib/utilities');
	
	globals.navBarColor       = '#94b735';
	globals.appBGColor        = '#eeefe1';
	globals.toolbarColor      = '#4396be';
	
	globals.logged = false;
	globals.SITE_URL = 'http://www.mycpohq.com/';
	globals.AUTH_KEY;
	globals.USER_TYPE;
	globals.projects  = {};
	globals.usertasks = {};
	globals.contacts  = {};
	globals.usertasks2 = {};
	
	globals.projectPicker   = Ti.UI.createPicker();
	globals.projectPicker2  = Ti.UI.createPicker();
	
	globals.taskPicker      = Ti.UI.createPicker();
	globals.taskPicker2     = Ti.UI.createPicker();
	
	globals.btnDropTask;
	globals.pickerSlideUp   = Ti.UI.createAnimation({bottom: 0});
	globals.pickerSlideDown = Ti.UI.createAnimation({bottom: -260});	
	
	globals.ai = globals.utils.activityIndicator(); 
	
	database.createDatabase();
	
	globals.loginTab = new AppTabGroup(
		{
			title: 'Sign in',
			window: new LoginWindow({title: 'Sign in', backgroundColor: globals.appBGColor, barColor: globals.navBarColor, tabBarHidden: true})
		},
		{
			title: 'Find my password',
			window: new ForgotLoginWindow({title: 'Find my password', backgroundColor: globals.appBGColor, barColor: globals.navBarColor, tabBarHidden: true})
		}
	);
	
	globals.login.tryAutoLogin();
	
	globals.spinUpApp = function() {				
		globals.ai._hide();	
		var TimerWindow       = require('ui/TimerWindow');
		var	TasksWindow       = require('ui/TasksWindow');
		var	ContactsWindow    = require('ui/ContactsWindow');
		var	AccountWindow     = require('ui/AccountWindow');
		var projects          = require('lib/projects');
		
		globals.tasks         = require('lib/tasks');
		globals.timesheets    = require('lib/timesheets');
		globals.files         = require('lib/files');
		globals.moment        = require('lib/moment');
		globals.tasksTabs     = Ti.UI.createTabGroup();
		
		globals.badgeTriggers = {
			timers: 0,
			messages: 0,
			tasks: 0
		}		
		
		var now = globals.moment();
		
		globals.tabs = new AppTabGroup(
			{
				title: 'Timer',
				icon: 'images/icon_timer.png',
				window: new TimerWindow({title: now.format('MMM D, YYYY'), backgroundColor: globals.appBGColor, barColor: globals.navBarColor})
			},
			{
				title: 'Tasks',
				icon: 'images/icon_checkmark.png',
				window: new TasksWindow({ title: 'Tasks', backgroundColor: globals.appBGColor, barColor: globals.navBarColor})
			},
			{
				title: 'Account',
				icon: 'images/icon_user.png',
				window: new AccountWindow({title: 'Account', backgroundColor: globals.appBGColor, barColor: globals.navBarColor})
			}
			/*
			{
				title: 'Contacts',
				icon: 'images/icon_profile.png',
				window: new ContactsWindow({title: 'Contacts', backgroundColor: globals.appBGColor, barColor: globals.navBarColor})
			}
			*/
		);
				
		var pickerData = [];
		/*
		 * Add blank row to picker - this prevents the stupid TI error.
		 */ 
		pickerData.push(Ti.UI.createPickerRow({ title: '' }));
		
		globals.projectPicker.add(pickerData);
		globals.projectPicker2.add(pickerData);		
		globals.taskPicker.add(pickerData);
		globals.taskPicker2.add(pickerData);
						
		globals.tabs.open();
		if (globals.projects.length>0) {
			
		} else {
			globals.projects = projects.getProjectsForSelect();
		}	
		
		/*
		 * Clear app databaase
		 */
		globals.timesheets.clearTimesheets();
		
		Ti.UI.iPhone.appBadge = '';		
		Ti.App.Properties.setString('timers', globals.badgeTriggers.timers);
		Ti.App.Properties.setString('messages', globals.badgeTriggers.messages);
		Ti.App.Properties.setString('tasks', globals.badgeTriggers.tasks);
		var service = Ti.App.iOS.registerBackgroundService({
        	url:'backgroundService.js'
        });
	}
	
})();